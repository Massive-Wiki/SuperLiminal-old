# John Abbe and Peter Kaminski

Talked 2022-11-18, 2022-11-22, 2022-11-29, 2022-12-06, 2022-12-10
- John will check w Jonas re transcript extraction (what was I going to check?) update: she did a review of >100 descriptions &/or transcripts from a podcast
- Pete will be asking John about [[co-intelligence]] for future Plex newsletters
- John, Dounia, and Charles likely to have a call soon
- Pete to consider how/where else to intro John

## Obsidian

- https://twitter.com/NikkiSiapno/status/1593882400983072769/

## Next call

Move private stuff to SubLiminal (oops)
- Opal - focused, git-baked-in Pete formulation for an Obsidian replacement
- Zirconia
- Obsidian vault = git repo = MassiveWiki wiki

[DWeb holiday gathering](https://www.eventbrite.com/x/483843959307) of interest? (signed up) 
Beloved Groupware
Nurturing nurturers practice/support group

closing tabs habit establishment mutual support:
- places to put (aka, external brain)
    - Pinboard (archiving worth $?)
    - web page to pdf and store independently
    - different treatments depending on topics
    - critical: clarity which topics
- cleanup during
- periodic garbage collection

# Juiciest topics

aka, our rolling agenda or at least source for agenda-building. Many overlap with Plex topics/groups, co-intelligence network likely. What other nexuses (nexi?) with inter-group potential?

## This running conversation itself

...about what the juice has been, is, might be in these conversations between Pete and John

(John may have cut and then not pasted our prior notes on this. Did either of us copy older versions of this?)

## People/groups we know in common, people/groups we have or would like to connect the other with or at least let them know about

Multiple sections relevant below. **Any desire to keep linear notes from some/all calls?** If so, should be copied/pasted here rather than cut/pasted.

intro John to [Alexar Pendashteh](https://au.linkedin.com/in/pendashteh)

Jerry: Rel8, OGM

Klaus Mager: food systems, contacts in many places (Disney, etc.)

Mark Trexler: [climateweb](https://www.theclimateweb.com/) (Jerry put John in touch with him before, but have not interacted much)

### Groups

#### Plex - [Biweekly Plex Dispatch](https://plex.collectivesensecommons.org/) (Pete newsletter)
(Nov 16 has debrief w Charles Blass, John listened to some, attention wandered)

Plex is Pete's term for:
* Lionsberg
* Cicolab - Charles Blass and Lauren Nignon - weekly calls, big knowledge base. Broke up.
* Flotilla
* MassiveWiki
* OGM
* [Catalist.network](https://www.catalist.network/) (Vincent Arena) - community directory - Airtable and Bubble 
* Free Jerry's Brain

and CSC?

## Internet/web commons-based tools & inquiry

**The ones we assume 'everyone' online knows/has-access-to**/understands 'enough,' and the **inquiry of which additional tools** might be the best next steps to strengthen/extend the commons. ("Everyone" because of course it is not truly everyone.)

### We assume already that people know:

#### [email](https://www.fastmail.help/hc/en-us/articles/1500000278382-Email-standards) - software / service, and typical uses

including mailing lists

and newsletters! Inquiry: Is there a free/open or good enough indie alternative to Substack? (MailChimp, Mad Mimi, etc.)

#### Internet addresses (incl DNS, web, and email)

Fellowship of the Link - Pete, and:
- [sj](https://cyber.harvard.edu/people/sklein) - Masto (and general) optimist)
- Chris Aldrich (Mastodon expert) had a day's worth of great coding that they seemed unlikely to take up

copying/pasting links as URLs, markdown, etc.

John once developed a decent draft curriculum outline but lost it, could redevelop (done it many times in my head)

#### the World Wide Web

software, and typical uses

bookmarking - underutilized

fracture of web services among browsers as a  problem/opportunity - e.g., Firefox syncs bookmarks and other things nicely, presumably Chrome, Opera, others can as well but they don't interoperate. Some such have existed, not sure of current status.

Imagine if Hypothes.is (or equivalent) were built into every major web browser?

### Potential next things-everyone-knows/has
...and each of these is a juicy topic in its own right.

#### Feeds aka news aka blogging incl podcasts and especially right now, microblogging via fediverse/ActivityPub

Could open space / brainstorm more, but:
- Shannon's initiative on paid Fediverse hosting
- Dave Sifry also?
-  John - guide - [gathering links](https://pinboard.in/u:johnabbe/t:mastodon), radio show on KEPW? [thinking on this keeps evolving - private weekly call that's more general, public call with groupware frame which would initially focus on Mastodon & the fediverse - someone really needs to start a band by this name --John, 12/9/2022]
    - others doing this, and/or podcasts?
    - https://eqpa.wordpress.com/2022/11/27/peak-twitter-is-behind-us/
    - https://eqpa.wordpress.com/2022/11/14/how-many-toots-in-a-trumpet/
- ???

#### "Local" (physical and/or virtual) co-ops and local leadership support for neighborhood (~100-1000) neighborhoods' Internet/computer infrastructure
- a la Bob Frankston's itch re Internet service
- consider each group 'in' CSC as such a group?
- John - connections with Eugene civic tech, e.g. eugene.social
- It's a use case for Ridiculously Easy Group Forming, one of many threads that got John to ->
#### Ridiculously Easy *Inter-group* Forming (12/7 popped into John's brain re what we are going for with:
    - the various Plex and esp.? CSC groups
    - existing and possible CII groups/projects, and for sure upcoming connection/outreach to others
    - also see NGL
    - John - UO leftish people/groups did this oamong themselves and , would like to explore again/further. From directories to the Fred Hampton approach.
- others?

### git, or NoNameYet

git may 'fit' better as one of many tools which overlap quite a bit in function and make a larger whole with perhaps no good name/label as yet. Some "enough" conglomeration of good tools for collaboratively gathering, organizing, versioning, curating, presenting, and/or sharing things.

#### Structure across documents within a site and across sites

URLs solve a lot of problems, but are very ugly and long. Distracting.

WikiCase within, needs more markup to 

#### Structure within documents

code - lines are chunks, text - not so much. Some kind of structure solves this for many uses within a site, including transclusion both within and across sites. (federated wiki)

##### solve with markup headers
first h1 is title
the rest appoximate outlining

##### solve w programmatic chunking

Outliners

WordPress (and others) use blocks

Decko (nee Wagn) uses cards

#### text styling

markup

storage for consistency - markup, HTML (experience w Decko)

#### info store

- git
    - alternatives such as [Pijul](https://pijul.org/), [Syncthing](https://syncthing.net/)
    - different forges
- wikis
- collaborative writing (e.g., HackMD), by themselves or as a feature in a compound tool
- commons-based Office/GDrive - e.g., CloudAmo
- ??

#### Live collaborative editing

Does anything solve this built in? Pattern now is to copy to something like HackMD and then copy back afterwards. Very sub-optimal.

Pete has more?

## Juicy? In our parking lot, anyway

Upcoming dates with possibilities:

- March 25 is WikiBirthday
- 2030s Centenary of Uncertainty

Evergreen:
- Personal growth, self-actualization, NVC, **Mutual Support**, human rights in mental health
- Professionalism
- Project Mushroom (John is in!)
- ChatGPT - Pete still super interested but doesn't need to talk about it
    - remembers your past conversations, but not others'
    - also the whole strain of text-to-fill-in-the-blank AI
- Wendy Elford
- directories, **directory(ies) of directories**
- recursion, [meta](https://eqpa.wordpress.com/2022/12/04/i-finally-met-a-meta-i-didnt-like/)
- the embodied-abstract rhythm - e.g., theory u
- 
People we want each other to know about
Aaron Swartz
commons, and governance/economics more generally
https://pinboard.in/u:johnabbe/t:commons
systems (as with Wendy, and more?)
international development
founder(s) of the US National Weather Service


Random bits (feel free to integrate above/below):
- re Git: https://gregoryszorc.com/blog/2017/12/11/high-level-problems-with-git-and-how-to-fix-them/


# 2022-11-29

- √images:
    - unsplash, openverse (incl Flickr CC?)
    - pixabay
    - pexels
- √Fellowship of the link:
    - [sj](https://cyber.harvard.edu/people/sklein) (Masto (and general) optimist)
    - Chris Aldrich (Mastodon expert) had a day's worth of great coding that they seemed unlikely to take up
- international development work issues
    - issues w Gates Foundation, etc.
- GroupWorks - John's new cards:
    - Right Person in/for Right Role (fits with [Seasoned Timing](https://groupworksdeck.org/patterns/Seasoned_Timing), [Right Size Bite](https://groupworksdeck.org/patterns/Right_Size_Bite))
    - Already Doing It
- AI/AGI
    - ChatGPT
        - √remembers your past conversations, but not others'
- Books
    - [Accelerando](https://en.wikipedia.org/wiki/Accelerando)
- Flotilla club - inter-Maker salon - 4-5 core, 4-5 more
    - Jordan? How to change the world to be a better place
        - small decentralized teams and how they federate, interoperate (need/offer matching, people w tools that can assist) - failed in making a collective brain space about it
        - industry group for makin g directories, and matchmaking 
    - casual, and can think deeply very fast
    - Vincent
    - Michael Grossman - Facrot - shared bmarking
    - Wendy Mc;lean - ways of mapping human interaction - Tapestry (soft brand), Everone's Wisdom hard brand)
    - Group thinks differently than any one of them
    - Michael contrasted with CTA - Collaborative Technology Alliance, bigger but less ability to think deeply
    - [FlotillaWiki](https://github.com/Flotilla-Tools-for-Connectors/Flotilla-Wiki)
- MassiveWiki
- ActivityPub opportunities (ours and others)
    - see notes from last call, we listed some things but did not dig in
    - Mastodon 'share' links for email, web?
- How these conversations continue
    - More on "The juice" - between us that seems to lead to another and another call
- Mastodon
    - Chris' WP blog does ActivityPub
    - https://maya.land/responses/2022/11/28/we-live-in-a-society.html
    - Groups on Mastodon
        - John in email: "For a Fediverse/Mastodon group, I was thinking something like @HolisticTech or @PeopleAndTech@a.gup.pe - or whatever name catches our intent, but probably not JerrysRetreat since these groups are open to anyone joining."

# 2022-11-29

Agenda:
- personal
- Plex/Lionsberg (Pete's the guru) ~shambles at the moment, but interesting stuff among Social Dimensions people
- HackMD and other tools
- "The juice" - between us that seems to lead to another and another call
- ActivityPub opportunities (ours and others)
- How these conversations continue

## The 'personal' - mutual support, personal/group 'self-actualization'

- general - importance of this stuff in any group or work or anything
- [Stutz](https://www.netflix.com/title/81387962) and [The Tools](https://www.thetoolsbook.com/about-phil-and-barry) - Life Force - body, people/relationships, self

led into:

## ??/Lionsberg (Pete's the guru) ~shambles at the moment, but interesting stuff among Social Dimensions people:

- Pete's friends w Jordan Sukut - shepherd - convenes, superpower is helping you feel like you've all joined for a reason (even when that isn't actually so clear)
    - Best MassiveWiki user, ad hoc hypertexts everywhere. Obsidian*
    - cosmology writing (porting to MW)
        - please ask him re [BigOmega](https://www.reddit.com/r/omnitheism/comments/48ct4f/big_omega/) - from c2 wiki -> WhyClublet - the first Christian wiki, split off Ward's when that content was too much there. "It has been described as a failure. Between September 1, 2005 and November 11, 2005, its recent changes page showed a total of 17 posts." - www . conservapedia . com/Christian_Wikis
- ("gang" Venns w Lionsberg, others:)
    - Erik Willekens
    - Killu Sanborn -facilitator, Magic
        - https://groupworksdeck.org/patterns/Magic
    - some more folks: Michael, Bill, Judy, Wendy E, Wendy M, Vincent
- Social Dimensions rebooting
    - (plus Grace - crypto white papers)
- gang's interests/foci:
    - text manipulations diretories (Flotilla), this one is finishing stuff
    - interop between tech platforms
    - self/group actualization (see personal above)

## HackMD and repository/organizing tools - docs and folders, and/or wiki/other links among them
- [Hedge Doc](https://hedgedoc.org/)
- CryptPad - E2E?
- [NextCloud](https://nextcloud.com/) (founder's fork of OwnCloud) - MarkDown and collaborative, https & own server?
    - [CloudAmo](https://cloudamo.com/store/nextcloud) - public offering of it (paid, $3-4 lowest tier)

### Git

#### Forges
- GitHub - now owned by Microsoft, need we say more?
- GitLab - the primary "not-Microsoft", kinda open option
- Bitbucket - commercial by Atlassian, it's just a smaller version of GitHub
- sr.ht - aggro open source, a little beta
- Codeberg

#### Forge Software (for self-hosting)

- [Gitea](https://gitea.io/en-us/)
- others like Gogs, etc.

Pete's buddy trying on this is Bill Anderson - in his 70s, Xerox, chemistry, computer guy, has a hot shit programmer friend from the '70s

(find/add a section about people)

#### √Alternatives to Git

- [Pijul](https://pijul.org/)
- [Syncthing](https://syncthing.net/)


## √ActivityPub opportunities (ours and others)
- open space / brainstorm more, but:
- John guide - [gathering links](https://pinboard.in/u:johnabbe/t:mastodon) - need a tool for publishing Pinboard selections! (Have to be logged in to see anything?)
- John - Fediverse radio show on KEPW?
    - others doing this, and/or podcasts?
    - https://eqpa.wordpress.com/2022/11/27/peak-twitter-is-behind-us/
    - https://eqpa.wordpress.com/2022/11/14/how-many-toots-in-a-trumpet/
- Shannon's initiative on paid Fediverse hosting
- Sifry also?
- ???



# 2022-11-18

## What's Going On?

### Twitter meltdown
- [Former Twitter employees fear the platform isn't long for the world \| MIT Technology Review](https://www.technologyreview.com/2022/11/18/1063467/former-twitter-employees-fear-the-platform-might-only-last-weeks/) (see earlier articles as well)

(later - doesn't seem to be panning out, beyond beyond some dropped?/stuttering features)

### Mastodon
- can use tags more
    - search not cross-instance, but search engines works pretty well (find post)
    - 'no' algorithm (though reverse time order actually is one)
- https://pinboard.in/u:johnabbe/t:mastodon
- https://pinboard.in/t:mastodon
- https://pinboard.in/u:johnabbe/t:mastodon/t:introduction
- [CSC Mastodon Resources](https://hackmd.io/RwFFEoK1RUSbUBTnb7Ahuw?both) (Pete started)

### Check-in / catching up

John is back at Walnut Street Co-operative. CII/CI work - online 'place', in spirit of groupware = processes and tech to support (PeterTrudy)

Pete: decentralization, federation -- both with human process and tech
harmonizing

√Biweekly Plex Dispatch - Pete newsletter
- https://plex.collectivesensecommons.org/
- latest has conversation w Charles Blass

√Plex is Pete's term for:
* Lionsberg
* Cicolab - Charles Blass and Lauren Nignon - weekly calls, big knowledge base. Broke up.
* Flotilla
* MassiveWiki
* OGM
* [Catalist.network](https://www.catalist.network/) (Vincent Arena) - community directory - Airtable and Bubble 
* Free Jerry's Brain

√Jerry: Rel8, OGM

√Klaus Mager: food systems, contacts in many places (Disney, etc.)

√Mark Trexler: [climateweb](https://www.theclimateweb.com/)


[Collaborative Technology Alliance](https://www.collaborative.tech/) (where/how does this fit? --John)

