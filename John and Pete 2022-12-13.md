# John and Pete 2022-12-13

notes from https://hackmd.io/V4qX-Bj6RSO3xqUgUcHlbQ?both

## Done
- masks for COVID, facial hair
- shaving cream: [Pacific Shaving Company Natural Shaving Cream](https://www.amazon.com/gp/product/B00DKET54U/)
- OGM list - bubbling w activity right now
	- John finally signed up
    - Grace Sense-making sense-making, Covid as example
    - Rob pointed out flow-mode of email list hard to  do that with
    - Pete - Marc-Antoine willing to apply IdeaLoom, needs a team of 2-3 to steward. Nancy W a candidate!
- IdeaLoom for OGM - 
    - Marc-Antoine - Information Architect Architect
        - https://www.idealoom.org/pages/features.html
        - good at processing flows like email list conversations into stocks
    - Concept Maps, Topic Maps - Jack Park
    - Jeff Conklin
    - answer from Nancy White: "Sounds good. Lucas Cioffi at qiqochat is interested in participating in using it (we are looking at similar challenges in the Liberating Structures community so I see this as a two community learning opportunity for me/us). I could talk this week or january. This week tomorrow except between 11 - 12:30, Thursday and Friday if really early (like 7-8am pst) otherwise could take it in the car as we drive up to cabin around 8:45 - 10."
	    - John is interested, even moreso for the chance to work w Nancy
- Doc's FLOSS Weekly Podcast tomorrow
    - wise, healthy?
    - will the iron be as hot or nearly as hot later?
    - more generally, what is the juice Pete sees?
    - for Pete, Doc and co-host Shawn Powers were friendly, informed themselves well, and engaging, and made it easy
    - [FLOSS WEEKLY 695 MASSIVE WIKI, 2022-08-24](https://twit.tv/shows/floss-weekly/episodes/695) and [Transcript for FLOSS WEEKLY 695 MASSIVE WIKI](https://twit.tv/posts/transcripts/floss-weekly-695-transcript)


### What will John focus on?

- **Ridiculously Easy Inter-group Forming**
    - MassiveWiki - principles that support that
        - Collective Sense-making Commons - more tools
            - https://collectivesensecommons.org/
        - -> Massive Human Intelligence
            - https://massivehumanintelligence.org/
    - Co-Intelligence Institute
    - Fred Hampton's work
    - local Eugene community
- Ridiculously Easy Inter-group Forming as a riff on...:
	- Ridiculously Easy Group Forming
	- [Seb's first? post on it](https://web.archive.org/web/20021210023853/http://radio.weblogs.com/0110772/2002/10/09.html)
	- https://www.fastcompany.com/664653/ridiculously-easy-group-forming (2004)
	- http://tantek.com/presentations/2004sxsw/xfn.html (2004)
	- https://jasonlefkowitz.net/2002/10/ridiculously_ea/ (2002)
	- https://web.archive.org/web/20021201030904/http://www.myelin.co.nz/cgi-bin/wcswiki.pl?RidiculouslyEasyGroupForming (2002)
	- Topic Exchange
	- [[hashtags]]

### Email

We wrote this and sent it (see below), they had already decided on a no-guests show for tomorrow but have signed John up for the Feb 15 show

Hi Doc, I gather you are looking for a guest for tomorrow and I am offering myself!  

I would bring my experience as an on-and-off recovering coder - since the first time in high school, after burning out in 1980 on coding the user interface for the first robotic wafer sorter at ADE, my Dad's test equipment company. In college I broke down and learned Lisp and C. Around 1990, Richard Stallman cornered me at an SF convention and annoyed me whining about how evil Apple was, but ultimately convinced me how critical free software is to positive human use of technology going forward.  

Since then I was a relatively early participant in the wiki & blogging communities; did a deep dive with Nonviolent Communication; lived in Sri Lanka for a few years; moved into a co-operative in Eugene, Oregon in 2006; served on the board of the Co-Intelligence Institute; was involved a bit in Occupy Eugene and later Standing Rock (started & modded r/NoDAPL); was part of a 2014 cross-country march for climate action, then was unhoused for a few years during which I also helped a low-power radio station start up  (KEPW 97.3FM), and am now back again living in community and helping others and myself figure out Mastodon & the Fediverse. (Has a band grabbed that name yet?)  

Throughout, I have remained interested in tech even when I was not programming, often being 'the geek' others turn to when they want to understand something or need help. And fascinating or boring them with my rants on various related topics.  

I'd be happy to talk about any of the above. One main focus of mine right now is Ridiculously Easy Inter-group Forming, an extension of Seb Paquet et al's Ridiculously Easy Group Forming of the early 2000s. That means everything from Pete Kaminski's work on MassiveWiki and such, to the Co-Intelligence Institute as it transitions from inward focus to connecting more with other organizations innovating and helping hold democratic assemblies, to learning more about Fred Hampton's work among different activist groups in Chicago.  

Let me know if you'd like to have me on the show - tomorrow or at any future date. You can also reach me at 617-678-3155, please message first (ideally Signal).  

Life,
John  

Upcoming re the Co-Intelligence Institute:
[https://www.co-intelligence.institute/community](https://www.co-intelligence.institute/community)  

"If you don't like the news, go out and make some of your own."
    --Wes Nisker




# Ahead
- SuperLiminal / SubLiminal setup - rename existing to SubLiminal, then set up SuperLiminal and move public stuff over
- getting Bill (and Matthew - Tools for Thinking Map Project) into superliminal / subliminal tomorrow
    - Massive Wiki Wednesday
        - 9am PT Wednesdays
        - https://chat.collectivesensecommons.org/agora/channels/massive-wiki
        - https://us02web.zoom.us/j/84876942036?pwd=amVrWVBmRW45RDBoTGE0bmFIQzFKUT09
        - https://massive.wiki/massive_wiki_meetups
        - also see https://meta.wikimedia.org/wiki/Wiki_Wednesday
- Fellowship of the Link call Wednesdays at 11am

- Framing of our work together (juicy conversation -> something coherent)
- cdent, now anticdent, question about outlines on Mastodon
- closing tabs habit establishment / mutual support:
- places to put (aka, external brain)
    - Pinboard (archiving worth $39? Slow would like to pay something anyway)
	    - web page to pdf and store independently?
    - different treatments depending on topics
    - critical: clarity which topics
- keeping it clean while in process
- periodic garbage collection likely necessary as well


## Misc notes

- Opal - focused, git-baked-in Pete formulation for an Obsidian replacement
- Zirconia
- Obsidian vault = git repo = MassiveWiki wiki

Beloved Groupware
Nurturing nurturers practice/support group



## Handled before call
[DWeb holiday gathering](https://www.eventbrite.com/x/483843959307) of interest? (signed up) - likely to be more casual, January may be more interesting