# SuperLiminal

Hello, world.

==

Are there any conventions for what goes in the README of a MassiveWiki?


someday transclude these:

*link principles here*


*link how-to here*
(already written down somewhere:)
In preferences:
Turn off titles
Install/Enable Obsidian Git plugin


[[WikiFeaturesMostWanted]]


# Understanding the command palette
(second-to-top right)

git add - putting one change into staging

git commit - chunk of changes describably as a one-liner

get to understanding fetch / pre-resolve thing






To be moved to appropriate page:

MW treats README as a special case, because in git* "README.MD" is the index.html of a repo folder (all the way down). The top level one is critical for the whole project. MW uses a file named the same as the name of the directory.


MassiveWiki - [principles](https://massive.wiki/massive_wiki_manifesto)

plus:
HackMD for *collaborative editing*

Small Things Loosely Joined


plaintext - line endings?


# Markdown minimal viable product:

## wiki structure
double brackets make links

## document structure
hashes for headers (line of dashes/equals works under, but...) 
dashes (or stars which is used for bold) for bullets



bold, italics (stars or underscores)

[[chunking, naming, linking]]

In MassiveWiki (as with most wikis), the chunk is a page.
Would like paragraph-sized chunks, for transclusion. We know that is very complex, in general and particularly for Markdown, so, what MassiveWiki does is use headers - h1 to h6 - as chunks. Specifically:
h1 - title of the page =~ file name of page


SEText style headers

---------------
Topic
------------------------



topic
===================

WikiCase